## Back end developer coding assessment

## Coding assessment description

You've received a request to implement an microservice with REST API 
to track Companies information, Contracts between them and Purchases 

## You are asked to

1. Create a database structure for Companies, Contracts and Purchases
2. Design and implement REST API for CRUD actions on Companies and Contracts
3. Design and implement REST API for creating new Purchases
4. Write unit tests and integration tests with reasonably high code coverage 

### What we expect from you

1. Don't spend too much time (>1 workday) solving this task
2. Show your testing skills by writing automated tests for your solution
3. Solve this task with scalability in mind. What if number of companies or contracts will be 100000 or 1000000?
4. Do your best writing clean & maintainable code
5. Demonstrate your software design skills (SOLID, KISS, DRY, etc)
6. Use DVCS Git, share your code via Bitbucket private repo (send invitation link to alex@demins.com)

### Technologies you have to use

1. PHP 7.x as a back end language and Laravel as a framework
2. MySQL as a database engine

It will be perfect if you'll put a whole application in a docker container/containers. 
If we'll be able to run it just typing `docker-compose up`, you will earn some extra points.  

### Data model and logic
Here is a brief data model you've received. 
You are free to add any additional fields and keys if you need them.

As you can see, each contract contains some number of initially issued credits. 
And every purchase is intended to track credits usage.

For example if you have contract with issued 10 credits, customer should be able to 
create 10 (ten) purchases with 1 credit each, or one purchase with all 10 credits, 
or two purchases with 5 credits each, or any other number of purchases with any number 
of credits. But total number of credits spent must be less or equal to issued number of credits.

Have race condition possibility in mind while implementing this task.

#### Company
1. Company name
2. Company registration code (alphanumeric, optional)

#### Contract
1. Seller company
2. Client company
3. Contract number (alphanumeric)
4. Date signed
5. Valid till date
6. Credits initially issued

#### Purchase
1. Affecting contract
2. Purchase date/time
2. Credits spent